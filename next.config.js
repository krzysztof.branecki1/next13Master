/** @type {import('next').NextConfig} */
const nextConfig = {
	// output: "standalone",
	experimental: {
		typedRoutes: true,
		mdxRs: true,
	},
	pageExtensions: ["ts", "tsx", "markdown", ""],
	images: {
		remotePatterns: [
			{ hostname: "media.graphassets.com" },
			{ hostname: "picsum.photos" },
			{ hostname: "loremflickr.com" },
			{ hostname: "img.clerk.com" },
		],
		// domains: ["media.graphassets.com", "img.clerk.com", "loremflickr.com", "picsum.photos"],
	},
	async redirects() {
		return [
			{
				source: "/products",
				destination: "/products/1",
				permanent: true,
			},
			{
				source: "/categories/:categorySlug",
				destination: "/categories/:categorySlug/1",
				permanent: true,
			},
			{
				source: "/collections/:collectionSlug",
				destination: "/collections/:collectionSlug/1",
				permanent: true,
			},
			// {
			// 	source: "/search",
			// 	destination: "/search/1",
			// 	permanent: true,
			// },
		];
	},
};

const withMDX = require("@next/mdx")();
module.exports = withMDX(nextConfig);

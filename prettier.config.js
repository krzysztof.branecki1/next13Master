module.exports = {
	semi: true,
	singleQuote: false,
	trailingComma: "all",
	printWidth: 120,
	useTabs: true,
	plugins: [
		"prettier-plugin-tailwindcss",
		"@trivago/prettier-plugin-sort-imports",
	],
	tailwindConfig: "./tailwind.config.ts",
	importOrder: [
		"<THIRD_PARTY_MODULES>",
		"^@ui/(.*)$",
		"^@atoms/(.*)$",
		"^@molecules/(.*)$",
		"^@organisms/(.*)$",
		"^@utils/(.*)$",
		"^[./]",
		"^[../]",
	],
	importOrderSeparation: true,
	importOrderSortSpecifiers: true,
};

import { SignUp } from "@clerk/nextjs";
import { ContentContainer } from "@layout/ContentContainer";

export default function Page() {
	return (
		<ContentContainer>
			<SignUp />
		</ContentContainer>
	);
}

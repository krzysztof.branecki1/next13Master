import { updateOrder } from "@/api/order/updateOrder";
import { PaymentStatus } from "@/gql/graphql";
import { getStripeCheckoutSession } from "@/lib/stripe";
import { ContentContainer } from "@layout/ContentContainer";
import Link from "next/link";

import { priceFormat } from "@utils/priceFormat";

export default async function CartSuccess({ searchParams }: { searchParams: { session_id: string } }) {
	const stripeCheckoutSession = await getStripeCheckoutSession(searchParams.session_id);
	const price = stripeCheckoutSession.amount_total;

	const status = stripeCheckoutSession.payment_status as PaymentStatus;
	const method = stripeCheckoutSession.payment_method_types;
	const orderId = stripeCheckoutSession.metadata?.order_id;
	if (status && orderId && stripeCheckoutSession.id) {
		await updateOrder(orderId, {
			stripeCheckoutId: stripeCheckoutSession.id,
			status: status,
			// email: stripeCheckoutSession.customer_details?.email,
		});
	}
	return (
		<ContentContainer>
			<h1 className="text-xl">Payment Success!</h1>
			<p>Amount: {!!price && priceFormat(price)}</p>
			<p>Status: {status}</p>
			<p>Method: {method.map((m) => m)}</p>
			<br />
			<Link
				href={"/orders"}
				className="inline-block sm:w-full lg:w-auto my-2 border rounded md py-4 px-8 text-center bg-indigo-600 text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-700 focus:ring-opacity-50"
			>
				Go to your orders
			</Link>
		</ContentContainer>
	);
}

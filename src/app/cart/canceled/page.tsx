import { getStripeCheckoutSession } from "@/lib/stripe";
import { ContentContainer } from "@layout/ContentContainer";
import { notFound } from "next/navigation";

export default async function CartSuccess({ searchParams }: { searchParams: { session_id: string } }) {
	if (!searchParams?.session_id) {
		return notFound();
	}

	const stripeCheckoutSession = await getStripeCheckoutSession(searchParams.session_id);

	return (
		<ContentContainer>
			Payment Cancelled!
			<br />
			{stripeCheckoutSession.url && (
				<a
					href={stripeCheckoutSession.url}
					className="inline-block sm:w-full lg:w-auto my-2 border rounded md py-4 px-8 text-center bg-indigo-600 text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-700 focus:ring-opacity-50"
				>
					Try again!
				</a>
			)}
		</ContentContainer>
	);
}

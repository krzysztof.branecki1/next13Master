import { handleStripePaymentAction } from "@/actions/handlePaymentAction";
import { getCart } from "@/api/getCart";
import { addOrder } from "@/api/order/addOrder";
import { EmptyCart } from "@app/cart/EmptyCart";
import { ContentContainer } from "@layout/ContentContainer";
import { Suspense } from "react";

import { ChangeQuantity } from "@atoms/ChangeQuantity";
import { RemoveButton } from "@atoms/RemoveButton";

import { priceFormat } from "@utils/priceFormat";

export default async function CartPage() {
	const cart = await getCart();

	if (!cart?.OrderItems || cart.OrderItems.length < 1) {
		return <EmptyCart />;
	}
	const addCustomerOrder = async () => {
		"use server";
		if (!cart) return;
		await addOrder(cart.id).then(async () => {
			await handleStripePaymentAction();
		});
	};
	return (
		<ContentContainer>
			<div className="px-4 sm:px-6 lg:px-8">
				<div className="mt-8 flow-root">
					<div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
						<div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
							<div className="overflow-hidden shadow ring-1 ring-black ring-opacity-5 sm:rounded-lg">
								<table className="min-w-full divide-y divide-gray-300">
									<thead className="bg-gray-50">
										<tr>
											<th
												scope="col"
												className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6"
											>
												Name
											</th>
											<th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
												Price
											</th>
											<th scope="col" className="px-3 py-3.5 text-center text-sm font-semibold text-gray-900">
												Quantity
											</th>
											<th scope="col" className="relative py-3.5 pl-3 pr-4 sm:pr-6">
												<span className="sr-only">View</span>
											</th>
										</tr>
									</thead>
									<tbody className="divide-y divide-gray-200 bg-white">
										{cart.OrderItems.map((item) => {
											const formatPrice = item?.Product ? priceFormat(item.Product?.price) : 0;
											if (!item?.Product) return null;
											return (
												<tr key={item.Product.id}>
													<td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">
														{item.Product.name}
													</td>
													<td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{formatPrice}</td>
													<td className="whitespace-nowrap px-3 py-4 text-sm text-center text-gray-500">
														<Suspense fallback={<div data-testid="quantity">{item.quantity} </div>}>
															<ChangeQuantity itemId={item.id} quantity={item.quantity} />
														</Suspense>
													</td>
													<td className="relative whitespace-nowrap flex justify-end gap-4 py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
														<a href={`product/${item.Product.slug}`} className="text-indigo-600 hover:text-indigo-900">
															View Product
															<span className="sr-only">, {item.Product?.name}</span>
														</a>
														<RemoveButton orderItemId={item.id} />
													</td>
												</tr>
											);
										})}
									</tbody>
								</table>
								<form action={addCustomerOrder} className="ml-auto">
									<button
										type="submit"
										className="rounded-sm border bg-slate-100 px-8 py-2 shadow-sm transition-colors hover:bg-slate-200"
									>
										Pay
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</ContentContainer>
	);
}

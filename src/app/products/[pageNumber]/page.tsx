import { getProducts } from "@/api/getProducts";
import { getProductsCount } from "@/api/getProductsCount";
import { ProductOrderByInput } from "@/gql/graphql";
import { PER_PAGE, defaultSortOrder } from "@/lib/consts";
import { ContentContainer } from "@layout/ContentContainer";
import { Metadata } from "next";
import { notFound } from "next/navigation";

import { SortOrder } from "@molecules/SortOrder";

import Pagination from "@organisms/Pagination";
import { ProductList } from "@organisms/ProductList";

export async function generateMetadata({ params }: { params: { pageNumber: string } }): Promise<Metadata> {
	return {
		title: `Products list - page ${params.pageNumber}`,
		openGraph: {
			title: `Products list - page ${params.pageNumber}`,
		},
	};
}

export const generateStaticParams = async () => {
	const productsCount = await getProductsCount();

	const numberOfPages = Math.ceil(productsCount / PER_PAGE);
	let productPages = [];
	for (let i = 0; i < numberOfPages; i++) {
		productPages.push({
			pageNumber: `${i + 1}`,
		});
	}
	return productPages;
};

export default async function Products({
	params: { pageNumber = "1" },
	searchParams,
}: {
	params: { pageNumber: string };
	searchParams?: { [_key: string]: string | string[] | undefined };
}) {
	if (Number.isFinite(pageNumber)) {
		return notFound();
	}
	let currentPage = parseInt(pageNumber);
	if (pageNumber !== currentPage.toString()) {
		return notFound();
	}

	const perPage = PER_PAGE;
	const { sort: sortValue } = searchParams as { [key: string]: ProductOrderByInput };
	const sortQuery = sortValue ?? defaultSortOrder;

	const response = await getProducts(currentPage, perPage, "", sortQuery);
	const totalItems = response.pagination.totalItems;

	return (
		<ContentContainer>
			<div className="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
				{response.products.length === 0 && <div className="mt-10">Products not found.</div>}
				{response.products.length > 0 && (
					<>
						<h2 className="text-2xl font-bold tracking-tight text-gray-900 flex justify-between">
							Products <SortOrder />
						</h2>
						<ProductList key={currentPage} products={response.products} />
						<Pagination baseUrl={"/products"} perPage={perPage} currentPage={currentPage} totalItems={totalItems} />
					</>
				)}
			</div>
		</ContentContainer>
	);
}

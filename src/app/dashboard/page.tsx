import { currentUser } from "@clerk/nextjs";
import { ContentContainer } from "@layout/ContentContainer";
import { RedirectType, redirect } from "next/navigation";

import { DeleteAccountForm } from "@molecules/DeleteAccountForm";
import { UpdateProfileForm } from "@molecules/UpdateProfileForm";

export default async function userSettings() {
	try {
		const user = await currentUser();
		if (!user) {
			redirect("/", RedirectType.replace);
		}
		return (
			<ContentContainer>
				<div>
					<main>
						{/* Settings forms */}
						<div className="divide-y divide-white/5">
							<div className="grid max-w-7xl grid-cols-1 gap-x-8 gap-y-10 px-4 py-16 sm:px-6 md:grid-cols-3 lg:px-8">
								<div>
									<h2 className="text-base font-semibold leading-7 ">Personal Information</h2>
									<p className="mt-1 text-sm leading-6 text-gray-400">
										Use a permanent address where you can receive mail.
									</p>
								</div>
								<UpdateProfileForm />
							</div>

							<div className="grid max-w-7xl grid-cols-1 gap-x-8 gap-y-10 px-4 py-16 sm:px-6 md:grid-cols-3 lg:px-8">
								<div>
									<h2 className="text-base font-semibold leading-7 ">Change password</h2>
									<p className="mt-1 text-sm leading-6 text-gray-400">
										Update your password associated with your account.
									</p>
								</div>

								<form className="md:col-span-2">
									<div className="grid grid-cols-1 gap-x-6 gap-y-8 sm:max-w-xl sm:grid-cols-6">
										<div className="col-span-full">
											<label htmlFor="current-password" className="block text-sm font-medium leading-6 ">
												Current password
											</label>
											<div className="mt-2">
												<input
													id="current-password"
													name="current_password"
													type="password"
													autoComplete="current-password"
													className="block w-full rounded-md border-0 bg-white/5 py-1.5  shadow-sm ring-1 ring-inset ring-white/10 focus:ring-2 focus:ring-inset focus:ring-indigo-500 sm:text-sm sm:leading-6"
												/>
											</div>
										</div>

										<div className="col-span-full">
											<label htmlFor="new-password" className="block text-sm font-medium leading-6 ">
												New password
											</label>
											<div className="mt-2">
												<input
													id="new-password"
													name="new_password"
													type="password"
													autoComplete="new-password"
													className="block w-full rounded-md border-0 bg-white/5 py-1.5  shadow-sm ring-1 ring-inset ring-white/10 focus:ring-2 focus:ring-inset focus:ring-indigo-500 sm:text-sm sm:leading-6"
												/>
											</div>
										</div>

										<div className="col-span-full">
											<label htmlFor="confirm-password" className="block text-sm font-medium leading-6 ">
												Confirm password
											</label>
											<div className="mt-2">
												<input
													id="confirm-password"
													name="confirm_password"
													type="password"
													autoComplete="new-password"
													className="block w-full rounded-md border-0 bg-white/5 py-1.5  shadow-sm ring-1 ring-inset ring-white/10 focus:ring-2 focus:ring-inset focus:ring-indigo-500 sm:text-sm sm:leading-6"
												/>
											</div>
										</div>
									</div>

									<div className="mt-8 flex">
										<button
											type="submit"
											className="rounded-md bg-indigo-500 px-3 py-2 text-sm font-semibold  shadow-sm hover:bg-indigo-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-500"
										>
											Save
										</button>
									</div>
								</form>
							</div>

							<div className="grid max-w-7xl grid-cols-1 gap-x-8 gap-y-10 px-4 py-16 sm:px-6 md:grid-cols-3 lg:px-8">
								<div>
									<h2 className="text-base font-semibold leading-7 ">Log out other sessions</h2>
									<p className="mt-1 text-sm leading-6 text-gray-400">
										Please enter your password to confirm you would like to log out of your other sessions across all of
										your devices.
									</p>
								</div>

								<form className="md:col-span-2">
									<div className="grid grid-cols-1 gap-x-6 gap-y-8 sm:max-w-xl sm:grid-cols-6">
										<div className="col-span-full">
											<label htmlFor="logout-password" className="block text-sm font-medium leading-6 ">
												Your password
											</label>
											<div className="mt-2">
												<input
													id="logout-password"
													name="password"
													type="password"
													autoComplete="current-password"
													className="block w-full rounded-md border-0 bg-white/5 py-1.5  shadow-sm ring-1 ring-inset ring-white/10 focus:ring-2 focus:ring-inset focus:ring-indigo-500 sm:text-sm sm:leading-6"
												/>
											</div>
										</div>
									</div>

									<div className="mt-8 flex">
										<button
											type="submit"
											className="rounded-md bg-indigo-500 px-3 py-2 text-sm font-semibold  shadow-sm hover:bg-indigo-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-500"
										>
											Log out other sessions
										</button>
									</div>
								</form>
							</div>

							<div className="grid max-w-7xl grid-cols-1 gap-x-8 gap-y-10 px-4 py-16 sm:px-6 md:grid-cols-3 lg:px-8">
								<div>
									<h2 className="text-base font-semibold leading-7 ">Delete account</h2>
									<p className="mt-1 text-sm leading-6 text-gray-400">
										No longer want to use our service? You can delete your account here. This action is not reversible.
										All information related to this account will be deleted permanently.
									</p>
								</div>

								<DeleteAccountForm />
							</div>
						</div>
					</main>
				</div>
			</ContentContainer>
		);
	} catch (error) {
		redirect("/");
	}
}

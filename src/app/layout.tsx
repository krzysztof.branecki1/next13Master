import { ClerkProvider } from "@clerk/nextjs";
import Header from "@layout/Header";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { ReactNode } from "react";

import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
	title: "Next13 Master",
	description: "Shop based on Next13",
};

export default function RootLayout({ children, modal }: { children: ReactNode; modal: ReactNode }) {
	return (
		<ClerkProvider publishableKey={process.env.NEXT_PUBLIC_CLERK_PUBLISHABLE_KEY}>
			<html lang="en">
				<body className={inter.className}>
					<Header />
					{children}
					<footer>
						<p className="text-center text-sm text-gray-500">© 2021 All rights reserved.</p>
					</footer>
					{modal}
				</body>
			</html>
		</ClerkProvider>
	);
}

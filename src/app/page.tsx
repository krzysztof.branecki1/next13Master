import { getProducts } from "@/api/getProducts";
import { ContentContainer } from "@layout/ContentContainer";
import Image from "next/image";

import { CollectionList } from "@organisms/CollectionList";
import { ProductList } from "@organisms/ProductList";

export default async function Home() {
	const response = await getProducts(1);

	return (
		<main className="flex min-h-screen flex-col items-center justify-between p-24">
			<div className="relative z-[-1] flex place-items-center before:absolute before:h-[300px] before:w-[480px] before:-translate-x-1/2 before:rounded-full before:bg-gradient-radial before:from-white before:to-transparent before:blur-2xl before:content-[''] after:absolute after:-z-20 after:h-[180px] after:w-[240px] after:translate-x-1/3 after:bg-gradient-conic after:from-sky-200 after:via-blue-200 after:blur-2xl after:content-[''] before:dark:bg-gradient-to-br before:dark:from-transparent before:dark:to-blue-700 before:dark:opacity-10 after:dark:from-sky-900 after:dark:via-[#0141ff] after:dark:opacity-40 before:lg:h-[360px]">
				<Image
					className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
					src="/next.svg"
					alt="Next.js Logo"
					width={180}
					height={37}
					priority
				/>
			</div>
			<CollectionList />
			<ContentContainer>
				<div className="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
					{response.products.length === 0 && (
						<div className="mt-10">Products not found.</div>
					)}
					{response.products.length > 0 && (
						<>
							<h2 className="text-2xl font-bold tracking-tight text-gray-900">
								Products
							</h2>
							<ProductList products={response.products} />
						</>
					)}
				</div>
			</ContentContainer>
		</main>
	);
}

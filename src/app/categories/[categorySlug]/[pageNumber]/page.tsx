import { getCategoryBySlug } from "@/api/getCategoryBySlug";
import { getCategoryProductsBySlug } from "@/api/getCategoryProductsBySlug";
import { PER_PAGE } from "@/lib/consts";
import { ContentContainer } from "@layout/ContentContainer";
import { Metadata } from "next";
import { notFound } from "next/navigation";

import Pagination from "@organisms/Pagination";
import { ProductList } from "@organisms/ProductList";

export async function generateMetadata({
	params,
}: {
	params: { pageNumber: string; categorySlug: string };
}): Promise<Metadata> {
	const categorySlug = params.categorySlug;
	const category = await getCategoryBySlug(categorySlug);

	return {
		title: category ? `${category.name} list - page ${params.pageNumber}` : "",
		description: category?.description ?? "",
		openGraph: {
			title: `${categorySlug} list - page ${params.pageNumber}`,
			description: category?.description ?? "",
		},
	};
}

export default async function Categories({
	params: { pageNumber = "1", categorySlug = "" },
}: {
	params: { pageNumber: string; categorySlug: string };
}) {
	if (Number.isFinite(pageNumber)) {
		return notFound();
	}
	let currentPage = parseInt(pageNumber);
	if (pageNumber !== currentPage.toString()) {
		return notFound();
	}

	const perPage = PER_PAGE;
	const baseUrl = `/categories/${categorySlug}`;
	const response = await getCategoryProductsBySlug(categorySlug, currentPage, perPage);
	const totalItems = response.pagination.totalItems;

	return (
		<ContentContainer>
			<div className="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
				{response.products.length === 0 && <div className="mt-10">Products not found.</div>}
				{response.products.length > 0 && (
					<>
						<h2 className="text-2xl font-bold tracking-tight text-gray-900">Accessories</h2>
						<ProductList key={currentPage} products={response.products} />
						<Pagination baseUrl={baseUrl} perPage={perPage} currentPage={currentPage} totalItems={totalItems} />
					</>
				)}
			</div>
		</ContentContainer>
	);
}

import { getCustomerOrders } from "@/api/customer/getCustomerOrders";
import { auth, currentUser } from "@clerk/nextjs";
import { ContentContainer } from "@layout/ContentContainer";
import { redirect } from "next/navigation";

import { OrdersList } from "@organisms/OrdersList";

export default async function Orders() {
	const AuthObject = auth();
	if (!AuthObject) {
		redirect("/sign-in");
	}
	try {
		const user = await currentUser();
		if (!user) {
			redirect("/sign-in");
		}
		const customer = await getCustomerOrders(user.id);
		if (!customer?.Orders) {
			return <ContentContainer>No orders</ContentContainer>;
		}
		return (
			<ContentContainer>
				<p>
					Hi, <strong>{user.firstName}</strong>
				</p>
				<OrdersList orders={customer?.Orders} />
			</ContentContainer>
		);
	} catch (error) {
		redirect("/sign-in");
	}
}

import { addProductToCartAction } from "@/actions/addProductToCartAction";
import { getProductBySlug } from "@/api/getProductBySlug";
import { getProducts } from "@/api/getProducts";
import { defaultSortOrder } from "@/lib/consts";
import { ContentContainer } from "@layout/ContentContainer";
import { Metadata } from "next";
import Link from "next/link";
import { notFound } from "next/navigation";
import { Suspense } from "react";

import { FavoriteButton } from "@atoms/FavoriteButton";
import { SingleProductCoverImage } from "@atoms/SingleProductCoverImage";
import { SocialIcons } from "@atoms/SocialIcons";

import { AddToCartButton } from "@molecules/AddToCartButton";
import { RatingStars } from "@molecules/RatingStars";
import { VariantList } from "@molecules/VariantList";

import { ReviewsList } from "@organisms/ReviewsList";
import { SuggestedProducts } from "@organisms/SuggestedProducts";

import { priceFormat } from "@utils/priceFormat";

export async function generateMetadata({ params }: { params: { productSlug: string } }): Promise<Metadata> {
	const product = await getProductBySlug(params.productSlug);

	return {
		title: product?.name,
		// description: product?.description,
		openGraph: {
			title: product?.name,
			// description: product?.description,
		},
	};
}

export async function generateStaticParams() {
	const res = await getProducts(1, 4, "", defaultSortOrder);
	const products = res.products;
	return products.map((product) => ({ productSlug: product.slug }));
}
export default async function Product({ params }: { params: { productSlug: string } }) {
	const product = await getProductBySlug(params.productSlug);
	if (!product) {
		return notFound();
	}
	const { name, images, description, price, productCollection } = product;

	const formatPrice = priceFormat(price);
	const headingClassName = "text-gray-900 text-3xl title-font font-medium mb-1";
	const image = images && images?.length > 0 ? images[0] : null;
	return (
		<ContentContainer>
			<section className="text-gray-600 body-font overflow-hidden">
				<div className="container px-5 my-8 mx-auto">
					<div className="lg:w-4/5 mx-auto flex flex-wrap">
						{image && <SingleProductCoverImage src={image.url} alt={name} height={400} width={400} />}
						<div className="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
							{productCollection && (
								<Link href={`/collections/${productCollection.slug}`}>
									<h2 className="text-sm title-font text-gray-500 tracking-widest">{productCollection.name}</h2>
								</Link>
							)}
							<h1 className={headingClassName}>{name}</h1>
							<div className="flex mb-4">
								<RatingStars rating={product.averageRating} />
								<SocialIcons />
							</div>
							<p className="leading-relaxed ">{description}</p>

							{/*<VariantList variants={variants} />*/}
							<div className="flex gap-4 mt-4">
								<span className="title-font font-medium text-2xl text-gray-900">{formatPrice}</span>
								<form action={addProductToCartAction}>
									<input type="hidden" name="productId" value={product.id} />
									<AddToCartButton />
								</form>
								<FavoriteButton />
							</div>
						</div>
					</div>
				</div>
			</section>
			<aside className="mx-auto max-w-2xs px-4 py-8 sm:px-6 sm:py-8 lg:max-w-7xl lg:px-8">
				<Suspense>
					<SuggestedProducts product={product} />
					{product.productReviews && <ReviewsList reviews={product.productReviews} productId={product.id} />}
				</Suspense>
			</aside>
		</ContentContainer>
	);
}

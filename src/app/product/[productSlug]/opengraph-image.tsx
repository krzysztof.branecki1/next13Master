import { getProductBySlug } from "@/api/getProductBySlug";
import { ImageResponse } from "next/og";

// Route segment config
// export const runtime = "edge";

// Image metadata
export const alt = "About Acme";
export const size = {
	width: 1200,
	height: 630,
};

export const contentType = "image/png";

// Image generation
export default async function OgImage({ params }: { params: { productSlug: string } }) {
	const product = await getProductBySlug(params.productSlug);
	if (!product?.images) return <div>Product not found</div>;
	const productImage = product?.images[0];
	return new ImageResponse(
		(
			// ImageResponse JSX element
			<div
				style={{
					width: "100%",
					height: "100%",
					display: "flex",
					alignItems: "center",
					justifyContent: "center",
				}}
			>
				{productImage && <img src={productImage.url} width={400} height={400} alt={product.name} />}
				<div
					style={{
						width: 800,
						display: "flex",
						flexDirection: "column",
						flexWrap: "wrap",
						paddingRight: 40,
					}}
				>
					<h1
						style={{
							fontSize: 54,
						}}
					>
						{product?.name}
					</h1>
					<p>{product.description}</p>
					{!!product.categories && <p>Category: {product.categories[0]?.name}</p>}
				</div>
			</div>
		),
		{
			...size,
		},
	);
}

import { SignIn } from "@clerk/nextjs";
import { ContentContainer } from "@layout/ContentContainer";

export default function Page() {
	return (
		<ContentContainer>
			<SignIn />
		</ContentContainer>
	);
}

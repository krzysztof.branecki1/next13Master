import { getCollectionBySlug } from "@/api/getCollectionBySlug";
import { getCollectionProductsBySlug } from "@/api/getCollectionProductsBySlug";
import { PER_PAGE } from "@/lib/consts";
import { ContentContainer } from "@layout/ContentContainer";
import { Metadata } from "next";
import { notFound } from "next/navigation";

import Pagination from "@organisms/Pagination";
import { ProductList } from "@organisms/ProductList";

export async function generateMetadata({
	params,
}: {
	params: { pageNumber: string; collectionSlug: string };
}): Promise<Metadata> {
	const collectionSlug = params.collectionSlug;
	const collection = await getCollectionBySlug(collectionSlug);

	return {
		title: collection?.name ?? "",
		description: collection?.description ?? "",
		openGraph: {
			title: `${collectionSlug} list - page ${params.pageNumber}`,
			description: collection?.description ?? "",
		},
	};
}

export default async function Collection({
	params: { pageNumber = "1", collectionSlug = "" },
}: {
	params: { pageNumber: string; collectionSlug: string };
}) {
	if (Number.isFinite(pageNumber)) {
		return notFound();
	}
	let currentPage = parseInt(pageNumber);
	if (pageNumber !== currentPage.toString()) {
		return notFound();
	}
	const perPage = PER_PAGE;

	const baseUrl = `/collections/${collectionSlug}`;
	// const productResponse = await getCollectionProductsBySlug(collectionSlug);
	const totalItems = 4;
	const collection = await getCollectionBySlug(collectionSlug);
	const products = collection?.products;
	return (
		<ContentContainer>
			{collection?.name && <h1 className="text-3xl font-extrabold tracking-tight text-gray-9">{collection?.name}</h1>}
			<div className="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
				{!products && <div className="mt-10">Products not found.</div>}
				{products && (
					<>
						<ProductList key={currentPage} products={products} />
						<Pagination baseUrl={baseUrl} perPage={perPage} currentPage={currentPage} totalItems={totalItems} />
					</>
				)}
			</div>
		</ContentContainer>
	);
}

import { createCustomer } from "@/api/customer/createCustomer";
import { IncomingHttpHeaders } from "http";
import { headers } from "next/headers";
import { NextResponse } from "next/server";
import { Webhook, WebhookRequiredHeaders } from "svix";

const webhookSecret = process.env.WEBHOOK_SECRET ?? "";

async function handler(req: Request) {
	const payload = await req.json();
	const headerList = headers();

	const heads = {
		"svix-id": headerList.get("svix-id") ?? "",
		"svix-timestamp": headerList.get("svix-timestamp") ?? "",
		"svix-signature": headerList.get("svix-signature") ?? "",
	};

	const wh = new Webhook(webhookSecret);
	let evt: Event | null = null;

	try {
		evt = wh.verify(JSON.stringify(payload), heads as IncomingHttpHeaders & WebhookRequiredHeaders) as Event;
	} catch (e) {
		console.error(e);
		return NextResponse.json({}, { status: 400 });
	}

	const eventType = evt.type;
	if (eventType === "user.created" || eventType === "user.updated" || eventType === "user.deleted") {
		const { id, email_addresses } = evt.data;
		await createCustomer(id, email_addresses[0]?.email_address).catch((error) => {
			return new Response(error, { status: 500 });
		});
		return new Response(null, { status: 204 });
	}
}

type Event = {
	data: {
		id: string;
		email_addresses: { id: string; email_address: string }[];
		username: string;
	};
	type: string;
	object: string;
};

export const GET = handler;
export const POST = handler;
export const PUT = handler;

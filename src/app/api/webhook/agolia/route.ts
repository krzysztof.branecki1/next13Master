import { indexProduct } from "@/lib/agolia";
import { NextRequest, NextResponse } from "next/server";

export async function POST(request: NextRequest): Promise<Response> {
	const { data } = await request.json();
	if (request.headers.get("Authorization") !== process.env.WEBHOOK_SECRET)
		return new Response("You don't have access!", { status: 401 });

	try {
		const result = await indexProduct(data.id);
		return NextResponse.json({ ...result }, { status: 201 });
	} catch (err) {
		return new NextResponse("Something went wrong!", { status: 500 });
	}
}

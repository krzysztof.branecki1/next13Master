import { getProductById } from "@/api/getProductById";
import { updateAverageProductRating } from "@/api/updateAverageProductRating";
import { HygraphHookRequest } from "@/lib/types";
import { revalidatePath } from "next/cache";
import { type NextRequest } from "next/server";

export async function POST(request: NextRequest): Promise<Response> {
	if (request.headers.get("authorization") !== process.env.WEBHOOK_SECRET)
		return new Response("You don't have access!", { status: 401 });

	const body = (await request.json()) as HygraphHookRequest;
	const productId = body.data?.product?.id;

	if (!productId) {
		return new Response("No product id", { status: 400 });
	}

	const product = await getProductById(productId, true);

	if (!product) {
		return new Response("Product not found", { status: 404 });
	}

	if (!product?.productReviews || product.productReviews.length === 0) {
		await updateAverageProductRating(product.id, null);
		console.log(`Revalidating /product/${product?.slug}`);
		revalidatePath(`/product/${product?.slug}`);
		revalidatePath(`/products/[pageNumber]`);
		return new Response("Product not have reviews", { status: 404 });
	}
	if (product?.productReviews && product?.productReviews?.length > 0) {
		const averageRating =
			product.productReviews.reduce((acc, review) => acc + (review.rating ?? 0), 0) / product.productReviews.length;

		await updateAverageProductRating(product.id, averageRating);
		console.log(`Revalidating /product/${product?.slug}`);
		revalidatePath(`/product/${product?.slug}`);
		revalidatePath(`/products/[pageNumber]`);

		return Response.json({ averageRating: averageRating }, { status: 201 });
	}

	return new Response(`Rating no updated for product with id ${productId}`, { status: 400 });
}

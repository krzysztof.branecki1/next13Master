import { HygraphHookRequest } from "@/lib/types";
import { revalidatePath } from "next/cache";
import { type NextRequest } from "next/server";

export async function POST(request: NextRequest): Promise<Response> {
	const body = (await request.json()) as HygraphHookRequest;
	const productSlug = body.data?.slug;
	if (productSlug) {
		console.log(`Revalidating /product/${productSlug}`);
		revalidatePath(`/product/${productSlug}`);
		console.log(`Revalidating /products`);
		revalidatePath(`/products`);
		console.log(`Revalidating products lists`);
		revalidatePath(`/products/[pageNumber]`);
		return new Response(null, { status: 204 });
	} else {
		return new Response(null, { status: 400 });
	}
}

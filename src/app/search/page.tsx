import { Metadata } from "next";

import Search from "./[pageNumber]/page";

export async function generateMetadata(): Promise<Metadata> {
	return {
		title: `Search Products list`,
		openGraph: {
			title: `Search Products list`,
		},
	};
}

export default Search;

import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { Dispatch, SetStateAction, useEffect, useState } from "react";

export const useProductVariablesSearchParams = (variantName: string): [string, Dispatch<SetStateAction<string>>] => {
	const searchParams = useSearchParams();
	const pathname = usePathname();
	const router = useRouter();
	const [variantValue, setVariantValue] = useState(searchParams.get(variantName) ?? "");

	useEffect(() => {
		const current = new URLSearchParams(searchParams);
		if (variantValue) {
			current.set(variantName, variantValue.toLowerCase());
			const searchParamsPath = current.toString();
			const newParams = searchParamsPath ? `?${searchParamsPath}` : "";
			// @ts-ignore
			router.replace(`${pathname}${newParams}`);
		}
	}, [variantValue]);
	return [variantValue, setVariantValue];
};

import { CollectionGetBySlugDocument } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";

export const getCollectionBySlug = async (
	slug: string,
	productFilter = {
		page: 1,
		perPage: 4,
		sortBy: "createdAt",
		sortOrder: "desc",
	},
) => {
	const graphqlResponse = await executeGraphql({
		query: CollectionGetBySlugDocument,
		variables: {
			slug,
			productsCount: productFilter.perPage,
			productsSkip: (productFilter.page - 1) * productFilter.perPage,
		},

		headers: { authorization: `Bearer ${process.env.HYGRAPH_QUERY_TOKEN}` },
	});
	console.log("graphqlResponse", graphqlResponse);
	if (!graphqlResponse.collection) {
		return null;
	}

	return graphqlResponse.collection;
};
// type getCollectionBySlugPromiseResponse = {
// 	name: string;
// 	description?: string | null;
// } | null;

import { ProductListItemFragment } from "@/gql/graphql";
import { getAgoliaIndex } from "@/lib/agolia";
import { SearchResponse } from "@algolia/client-search";

const index = getAgoliaIndex("products");

export const searchSuggestedProductsInAgolia = async (
	currentProduct: ProductListItemFragment,
): Promise<SearchResponse<ProductListItemFragment> | null> => {
	if (currentProduct?.categories && !currentProduct?.categories[0]) {
		return index.search<ProductListItemFragment>(currentProduct.name, {
			hitsPerPage: 4,
			filters: `id!=${currentProduct.id}`,
		});
	}
	return null;
	// return index.search<ProductListItemFragment>( currentProduct?.categories?[0].name, { hitsPerPage: 4 });
};

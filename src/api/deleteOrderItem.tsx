"use server";

import { OrderItemDeleteDocument } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";
import { revalidateTag } from "next/cache";

export async function deleteOrderItem(itemId: string) {
	await executeGraphql({
		query: OrderItemDeleteDocument,
		variables: {
			orderItemId: itemId,
			hash: `${crypto.randomUUID()}-${crypto.randomUUID()}`,
		},
		cache: "no-cache",
		headers: { authorization: `Bearer ${process.env.HYGRAPH_MUTATION_TOKEN}` },
	}).then(() => {
		revalidateTag("cart");
	});
}

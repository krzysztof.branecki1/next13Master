import { ProductListItemFragment, ProductOrderByInput, ProductsGetListDocument } from "@/gql/graphql";
import { PER_PAGE, defaultSortOrder } from "@/lib/consts";
import { executeGraphql } from "@/lib/executeGraphql";

type getProductListPromiseResponse = {
	products: ProductListItemFragment[];
	pagination: {
		totalItems: number;
		currentPage: number;
	};
};
export const getProducts = async (
	page: number,
	take = PER_PAGE,
	search = "",
	orderBy: ProductOrderByInput = defaultSortOrder,
): Promise<getProductListPromiseResponse> => {
	const offset = take * (page - 1);

	const graphqlResponse = await executeGraphql({
		query: ProductsGetListDocument,
		variables: {
			first: take,
			skip: offset,
			search,
			orderBy,
		},
		headers: { authorization: `Bearer ${process.env.HYGRAPH_QUERY_TOKEN}` },
	});

	if (!graphqlResponse.products) {
		return {
			products: [],
			pagination: {
				currentPage: page,
				totalItems: 0,
			},
		};
	}

	return {
		products: graphqlResponse.products,
		pagination: {
			currentPage: page,
			totalItems: graphqlResponse.productConnection?.total ?? 0,
		},
	};
};

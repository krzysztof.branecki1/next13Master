"use server";

import { OrderItemUpdateQuantityDocument } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";
import { revalidateTag } from "next/cache";

export async function changeItemQuantity(itemId: string, quantity = 1) {
	await executeGraphql({
		query: OrderItemUpdateQuantityDocument,
		variables: {
			orderItemId: itemId,
			quantity,
		},
		headers: { authorization: `Bearer ${process.env.HYGRAPH_MUTATION_TOKEN}` },
		cache: "no-cache",
	}).then(() => {
		revalidateTag("cart");
	});
}

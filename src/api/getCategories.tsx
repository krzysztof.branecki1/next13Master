import { CategoriesGetListDocument } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";

export const getCategories = async (
	first?: number,
	skip?: number,
): Promise<{ name: string; slug: string }[] | undefined> => {
	try {
		const graphqlResponse = await executeGraphql({
			query: CategoriesGetListDocument,
			variables: {
				first,
				skip,
			},
			headers: { authorization: `Bearer ${process.env.HYGRAPH_QUERY_TOKEN}` },
		});

		return graphqlResponse.categories;
	} catch (error) {
		console.error(error);
	}
};

import { OrderUpdateDocument, OrderUpdateInput } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";

export async function updateOrder(orderId: string, order: OrderUpdateInput) {
	if (!order) return;
	await executeGraphql({
		query: OrderUpdateDocument,
		variables: {
			orderId,
			order,
		},
		cache: "no-cache",
		headers: { authorization: `Bearer ${process.env.HYGRAPH_MUTATION_TOKEN}` },
	});
}

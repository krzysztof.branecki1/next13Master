import { OrderPublishDocument } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";
import { auth } from "@clerk/nextjs";

export async function addOrder(orderId: string) {
	const { userId } = auth();
	await executeGraphql({
		query: OrderPublishDocument,
		variables: {
			orderId: orderId,
			clerkId: userId,
		},
		cache: "no-cache",
		headers: { authorization: `Bearer ${process.env.HYGRAPH_MUTATION_TOKEN}` },
	});
}

import { ProductGetByIdDocument } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";

export const getProductById = async (id: string, withReview = false) => {
	const graphqlResponse = await executeGraphql({
		query: ProductGetByIdDocument,
		variables: {
			id,
			withReview,
		},
		headers: { authorization: `Bearer ${process.env.HYGRAPH_QUERY_TOKEN}` },
	});
	return graphqlResponse.product ? graphqlResponse.product : null;
};

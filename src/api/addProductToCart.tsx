import {
	OrderGetByIdQuery,
	OrderItemCreateDocument,
	OrderItemUpdateQuantityDocument,
	ProductGetByIdDocument,
} from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";
import { revalidateTag } from "next/cache";

export async function addProductToCart(cart: OrderGetByIdQuery["order"], productId: string) {
	const productInCart = cart?.OrderItems?.find((item) => item?.Product?.id === productId);

	if (!productInCart) {
		const { product } = await executeGraphql({
			query: ProductGetByIdDocument,
			variables: {
				id: productId,
			},
			headers: { authorization: `Bearer ${process.env.HYGRAPH_QUERY_TOKEN}` },
		});
		if (!product) {
			throw new Error(`Product with id ${productId} not found`);
		}
		if (!cart) {
			throw new Error(`Cart not found`);
		}
		revalidateTag("cart");
		await executeGraphql({
			query: OrderItemCreateDocument,
			variables: {
				orderId: cart.id,
				productId,
				total: product.price,
				quantity: 1,
				hash: `${crypto.randomUUID()}-${crypto.randomUUID()}`,
			},
			cache: "no-cache",
			hash: `${crypto.randomUUID()}-${crypto.randomUUID()}`,
			headers: { authorization: `Bearer ${process.env.HYGRAPH_MUTATION_TOKEN}` },
		});
	} else {
		revalidateTag("cart");
		await executeGraphql({
			query: OrderItemUpdateQuantityDocument,
			variables: {
				orderItemId: productInCart.id,
				quantity: productInCart.quantity ? productInCart.quantity + 1 : 1,
				total: 0,
			},
			cache: "no-cache",
			hash: `${crypto.randomUUID()}-${crypto.randomUUID()}`,
			headers: { authorization: `Bearer ${process.env.HYGRAPH_MUTATION_TOKEN}` },
		});
	}
}

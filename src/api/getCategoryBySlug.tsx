import { CategoryGetBySlugDocument } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";

export const getCategoryBySlug = async (slug: string) => {
	const graphqlResponse = await executeGraphql({
		query: CategoryGetBySlugDocument,
		variables: {
			slug,
		},
		headers: { authorization: `Bearer ${process.env.HYGRAPH_QUERY_TOKEN}` },
	});

	if (!graphqlResponse.category) {
		return null;
	}

	return graphqlResponse.category;
};
type getCategoryBySlugPromiseResponse = {
	name: string;
	description?: string | null | undefined;
} | null;

import { ProductGetBySlugDocument } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";

export const getProductBySlug = async (slug: string) => {
	const graphqlResponse = await executeGraphql({
		query: ProductGetBySlugDocument,
		variables: {
			slug,
		},
		next: {
			revalidate: 30,
		},
		headers: { authorization: `Bearer ${process.env.HYGRAPH_QUERY_TOKEN}` },
	});
	return graphqlResponse.product ? graphqlResponse.product : null;
};

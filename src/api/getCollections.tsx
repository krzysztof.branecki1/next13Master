import { CollectionsGetListDocument, CollectionsGetListQuery } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";

export const getCollections = async (first?: number, skip?: number) => {
	const graphqlResponse: CollectionsGetListQuery = await executeGraphql({
		query: CollectionsGetListDocument,
		variables: {
			first,
			skip,
		},
		headers: { authorization: `Bearer ${process.env.HYGRAPH_QUERY_TOKEN}` },
	});
	return graphqlResponse.collections;
};

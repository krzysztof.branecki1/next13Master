import { addCustomerToCart } from "@/api/customer/addCustomerToCart";
import { OrderGetByIdDocument, OrderGetByIdQuery, Stage } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";
import { auth } from "@clerk/nextjs";
import { cookies } from "next/headers";

export const getCart = async (withProducts = true): Promise<OrderGetByIdQuery["order"]> => {
	const cartId = cookies().get("cartId")?.value;
	if (!cartId) return null;
	const graphqlResponse = await executeGraphql({
		query: OrderGetByIdDocument,
		variables: {
			id: cartId,
			stage: Stage.Draft,
			withProducts,
			hash: `${crypto.randomUUID()}-${crypto.randomUUID()}`,
		},
		hash: `${crypto.randomUUID()}-${crypto.randomUUID()}`,
		cache: "no-store",
		next: {
			tags: ["cart"],
		},
		headers: { authorization: `Bearer ${process.env.HYGRAPH_QUERY_TOKEN}` },
	});
	const { userId } = auth();
	if (userId && !graphqlResponse.order?.Customer) {
		try {
			await addCustomerToCart(cartId, userId);
		} catch (err) {
			return null;
		}
	}
	if (!graphqlResponse.order && cartId) {
		cookies().delete("cartId");
		return null;
	}
	return graphqlResponse.order;
};

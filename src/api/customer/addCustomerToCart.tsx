import { OrderAddCustomerDocument } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";

export async function addCustomerToCart(orderId: string, clerkId: string) {
	if (!clerkId) return;
	return await executeGraphql({
		query: OrderAddCustomerDocument,
		variables: {
			orderId,
			clerkId,
		},
		headers: { authorization: `Bearer ${process.env.HYGRAPH_MUTATION_TOKEN}` },
	});
}

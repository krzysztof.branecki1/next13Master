import { CustomerGetOrdersDocument } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";

export const getCustomerOrders = async (clerkId: string) => {
	const graphqlResponse = await executeGraphql({
		query: CustomerGetOrdersDocument,
		variables: {
			clerkId,
		},
		cache: "no-store",
		next: {
			tags: ["orders"],
		},
		headers: { authorization: `Bearer ${process.env.HYGRAPH_QUERY_TOKEN}` },
	});

	return graphqlResponse.customer ? graphqlResponse.customer : null;
};

import { CustomerUpsertDocument } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";

export async function createCustomer(clerkId: string, _customerDefaultEmail?: string) {
	const response = await executeGraphql({
		query: CustomerUpsertDocument,
		variables: {
			where: {
				clerkId: clerkId,
			},
			upsert: {
				create: {
					clerkId: clerkId,
					// email: customerDefaultEmail,
				},
			},
		},
		headers: { authorization: `Bearer ${process.env.HYGRAPH_MUTATION_TOKEN}` },
	});
	return response.upsertCustomer;
}

import { EnumStage, OrderCreateDocument, PaymentStatus } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";
import { currentUser } from "@clerk/nextjs";
import { revalidateTag } from "next/cache";

export const createCart = async () => {
	const clerkUser = await currentUser();

	if (!clerkUser?.id) {
		const response = await executeGraphql({
			query: OrderCreateDocument,
			variables: {
				order: {
					quantity: 0,
					total: 0,
					status: PaymentStatus.Unpaid,
					Stage: EnumStage.Draft,
				},
			},
			hash: `${crypto.randomUUID()}-${crypto.randomUUID()}`,
			cache: "no-cache",
		}).then((res) => {
			return res;
		});
		if (response?.createOrder) {
			return response.createOrder;
		}
	} else {
		const response = await executeGraphql({
			query: OrderCreateDocument,
			variables: {
				// clerkId: clerkUser?.id,
				order: {
					// email: clerkUser?.emailAddresses[0]?.emailAddress,
					quantity: 0,
					total: 0,
					Customer: clerkUser?.id
						? {
								connect: {
									clerkId: clerkUser?.id,
								},
						  }
						: undefined,
					status: PaymentStatus.Unpaid,
					Stage: EnumStage.Draft,
				},
			},
			cache: "no-cache",
		}).then((res) => {
			console.log("CustomerWhereUniqueInput");
			revalidateTag("cart");
			return res;
		});

		if (response.createOrder) {
			return response.createOrder;
		}
	}

	return null;
};

import { getCart } from "@/api/getCart";
import { SignInButton, SignedIn, SignedOut } from "@clerk/nextjs";
import { ShoppingBagIcon } from "@heroicons/react/24/outline";
import Link from "next/link";

import ProfileDropdown from "@atoms/ProfileDropdown";

export default async function UserMenu() {
	const cart = await getCart();
	const cartTotal = cart?.OrderItems?.reduce((acc, item) => {
		return acc + (item?.quantity ?? 0);
	}, 0);

	return (
		<div className="flex gap-2 inset-y-0 right-0 items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
			<Link
				href={cartTotal && cartTotal > 0 ? "/cart/sidebar" : "/cart"}
				type="button"
				className="relative rounded-full bg-white p-1 text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
			>
				<span className="absolute -inset-1.5" />
				<span className="sr-only">View cart</span>
				<ShoppingBagIcon className="h-6 w-6" aria-hidden="true" />
				<span className="absolute top-3 right-0">{cartTotal}</span>
			</Link>
			<SignedIn>
				<ProfileDropdown />
			</SignedIn>
			<SignedOut>
				<SignInButton afterSignInUrl="/" />
			</SignedOut>
		</div>
	);
}

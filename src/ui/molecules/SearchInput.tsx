"use client";

import { usePathname, useRouter, useSearchParams } from "next/navigation";
import React, { useEffect, useState } from "react";

export default function SearchInput() {
	const pathname = usePathname();
	const router = useRouter();
	const searchParams = useSearchParams();
	const searchQuery = searchParams?.get("query");
	const [searchValue, setSearchValue] = useState(searchQuery ?? "");

	useEffect(() => {
		if (searchParams?.get("query")) {
			setSearchValue(searchQuery ?? "");
		} else {
			setSearchValue("");
		}
	}, [pathname]);

	useEffect(() => {
		const delayTimeout = setTimeout(() => {
			if (searchValue !== "") {
				const searchParams = searchValue ? `?query=${encodeURIComponent(searchValue)}` : "";
				router.push(`/search${searchParams}`);
			}
		}, 400);
		return () => {
			clearTimeout(delayTimeout);
		};
	}, [searchValue]);

	const onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
		setSearchValue(e.target.value);
	};

	const onSubmitForm = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
	};

	return (
		<form className="flex items-center" onSubmit={(e) => onSubmitForm(e)}>
			<input
				role="searchbox"
				onChange={(e) => onChangeInput(e)}
				type="text"
				name="query"
				placeholder="Search for products..."
				autoComplete="off"
				value={searchValue ?? ""}
				className="w-full rounded-lg border bg-white px-4 py-2 text-sm text-black placeholder:text-neutral-500 dark:border-neutral-800"
			/>
			<div className="absolute right-0 top-0 mr-3 flex h-full items-center"></div>
		</form>
	);
}

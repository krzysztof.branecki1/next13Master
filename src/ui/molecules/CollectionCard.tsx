import Link from "next/link";

export const CollectionCard = ({
	collection,
}: {
	collection: { name: string; slug: string; description?: string | null };
}) => {
	const { slug, name, description } = collection;
	const href = {
		pathname: `/collections/${slug}`,
	};
	return (
		<div className="p-4 md:w-1/3">
			<div className="flex rounded-lg h-full bg-gray-100 p-8 flex-col">
				<div className="flex items-center mb-3">
					<Link
						href={href}
						className="mt-3 text-indigo-500 inline-flex items-center"
					>
						<h2 className="text-gray-900 text-lg title-font font-medium">
							{name}
						</h2>
						<svg
							fill="none"
							stroke="currentColor"
							strokeLinecap="round"
							strokeLinejoin="round"
							strokeWidth="2"
							className="w-4 h-4 ml-2"
							viewBox="0 0 24 24"
						>
							<path d="M5 12h14M12 5l7 7-7 7"></path>
						</svg>
					</Link>
				</div>
				<div className="flex-grow">
					{description && (
						<p className="leading-relaxed text-base">{description}</p>
					)}
				</div>
			</div>
		</div>
	);
};

import { ChangeEvent } from "react";

import { Star } from "@atoms/Star";

export const InputRating = ({
	value,
	name,
	onChange,
}: {
	value: number;
	name: string;
	onChange: (e: ChangeEvent<HTMLInputElement>, name: string) => void;
}) => {
	return (
		<fieldset className="stars-rating flex flex-row-reverse justify-end">
			<input
				onChange={(e) => onChange(e, name)}
				checked={value === 5}
				className="sr-only"
				id="rating-5"
				type="radio"
				value="5"
				name="rating"
			/>
			<label htmlFor="rating-5">
				<Star isActive={value >= 5} size={5} />
			</label>
			<input
				onChange={(e) => onChange(e, name)}
				checked={value === 4}
				className="sr-only"
				id="rating-4"
				type="radio"
				value="4"
				name="rating"
			/>
			<label htmlFor="rating-4">
				<Star isActive={value >= 4} size={5} />
				<span className="sr-only">4 stars</span>
			</label>
			<input
				onChange={(e) => onChange(e, name)}
				checked={value === 3}
				className="sr-only"
				id="rating-3"
				type="radio"
				value="3"
				name="rating"
			/>
			<label htmlFor="rating-3">
				<Star isActive={value >= 3} size={5} />
				<span className="sr-only">3 stars</span>
			</label>
			<input
				onChange={(e) => onChange(e, name)}
				checked={value === 2}
				className="sr-only"
				id="rating-2"
				type="radio"
				value="2"
				name="rating"
			/>
			<label htmlFor="rating-2">
				<Star isActive={value >= 2} size={5} />
				<span className="sr-only">2 stars</span>
			</label>
			<input
				onChange={(e) => onChange(e, name)}
				checked={value === 1}
				className="sr-only"
				id="rating-1"
				type="radio"
				value="1"
				name="rating"
			/>
			<label htmlFor="rating-1">
				<Star isActive={value >= 1} size={5} />
				<span className="sr-only">1 star</span>
			</label>
		</fieldset>
	);
};

import { Star } from "@atoms/Star";

export const RatingStars = ({ rating }: { rating: number | null | undefined }) => {
	// if (!rating) return;
	const stars = [];
	for (let i = 1; i < 6; i++) {
		stars.push({
			key: i,
			active: !rating || i <= Math.round(rating),
		});
	}

	return (
		<div className="flex-shrink-0 flex-grow-0 flex items-center gap-2">
			<span data-testid="product-rating" className="small-caps text-xs">
				{rating ?? 5}/5
			</span>
			<div className="flex flex-shrink-0">
				{stars.map((star) => (
					<Star key={star.key} isActive={star.active} />
				))}
			</div>
		</div>
	);
};

"use client";

import { useFormStatus } from "react-dom";

import { Button } from "@atoms/Button";

export const AddToCartButton = () => {
	const formStatus = useFormStatus();
	return (
		<Button disabled={formStatus.pending} type="submit" data-testid="add-to-cart-button">
			Add to cart
		</Button>
	);
};

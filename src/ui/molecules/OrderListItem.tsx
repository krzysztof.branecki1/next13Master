"use client";

import { OrderFragment } from "@/gql/graphql";
import Image from "next/image";
import { useState } from "react";

import { OrderPaymentStatus } from "@atoms/OrderPaymentStatus";

import { dateFormat } from "@utils/dateFormat";
import { priceFormat } from "@utils/priceFormat";
import { totalOrderCost } from "@utils/totalOrderCost";

export const OrderListItem = ({ item }: { item: OrderFragment }) => {
	const [showProducts, setShowProducts] = useState(false);

	return (
		<>
			<tr className="cursor-pointer" onClick={() => setShowProducts(!showProducts)}>
				<td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
					<div className="">
						<p className="text-gray-900 whitespace-no-wrap">Vera Carpenter</p>
					</div>
				</td>
				<td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
					<p className="text-gray-900 whitespace-no-wrap">
						<time dateTime={dateFormat(item.createdAt)}>{dateFormat(item.createdAt)}</time>
					</p>
				</td>
				<td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
					<p className="text-gray-900 whitespace-no-wrap">
						<time dateTime={dateFormat(item.updatedAt)}>{dateFormat(item.updatedAt)}</time>
					</p>
				</td>
				<td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
					{/*<p className="text-gray-900 whitespace-no-wrap">{totalOrderCost(item?.OrderItems ?? [])}</p>*/}
				</td>
				<td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
					<p className="text-gray-900 whitespace-no-wrap">{item?.OrderItems?.length}</p>
				</td>
				<td className="px-5 py-5 border-b border-gray-200 bg-white text-sm text-center">
					<OrderPaymentStatus paymentStatus={item.status ?? "unpaid"} />
				</td>
			</tr>
			{showProducts && !!item.OrderItems && item.OrderItems.length > 0 && (
				<tr>
					<td colSpan={6} className="px-5 py-5 border-b border-gray-200 bg-white text-sm text-left">
						<ul>
							{item.OrderItems.map((orderItem) => (
								<li key={orderItem?.id}>
									<div className="flex items-center mb-5">
										<div className="flex-shrink-0 w-10 h-10">
											{orderItem?.Product?.images && (
												<Image
													className="w-full h-full rounded-full"
													src={orderItem.Product?.images[0]?.url ?? ""}
													width={40}
													height={40}
													alt={orderItem.Product?.name}
												/>
											)}
										</div>
										<div className="ml-3">
											<p className="text-gray-900 whitespace-no-wrap mb-1">{orderItem?.Product?.name}</p>
											<p className="text-gray-900 whitespace-no-wrap text-xs">
												Quantity: <strong>{orderItem?.quantity}</strong>
											</p>
											<p className="text-gray-900 whitespace-no-wrap text-xs">
												{/*Price: <strong>{priceFormat(orderItem?.total)}</strong>*/}
												Price: <strong>{priceFormat(0)}</strong>
											</p>
										</div>
									</div>
								</li>
							))}
						</ul>
					</td>
				</tr>
			)}
		</>
	);
};

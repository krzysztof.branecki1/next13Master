import addReviewAction from "@/actions/addReviewAction";
import { ReviewFragment } from "@/gql/graphql";
import { ChangeEvent, useState } from "react";

import Input from "@atoms/Input";
import Textarea from "@atoms/Textarea";

import { InputRating } from "@molecules/InputRating";

const initialState = {
	rating: 1,
	title: "",
	description: "",
	email: "",
	author: "",
};
export const ReviewForm = ({
	addReview,
	productId,
}: {
	productId: string;
	addReview: (pendingState: Omit<ReviewFragment, "id">) => void;
}) => {
	const [review, setReview] = useState(initialState);
	const handleChangeForm = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, name: string) => {
		if (name === "rating") setReview({ ...review, [name]: parseInt(e.target.value) });
		else setReview({ ...review, [name]: e.target.value });
	};
	const handleSubmit = async () => {
		addReview(review);
		setReview(initialState);
	};
	return (
		<>
			<h2 className="text-2xl font-bold tracking-tight text-gray-900">Customer Reviews</h2>
			<div className="mt-3 flex items-center">
				<div title="4.8 out of 5 stars">
					<div className="flex items-center"></div>
				</div>
			</div>
			<div className="mt-10">
				<h3 className="text-lg font-medium text-gray-900">Share your thoughts</h3>
				<p className="mt-1 text-sm text-gray-600">
					If you’ve used this product, share your thoughts with other customers
				</p>
				<form
					data-testid="add-review-form"
					action={addReviewAction}
					onSubmit={handleSubmit}
					className="mt-2 flex flex-col gap-y-2"
				>
					<input type="hidden" value={productId} name="productId" />
					<label>
						<span className="text-xs text-gray-700">Review title</span>
						<Input name="title" value={review.title} required={true} onChange={handleChangeForm} />
					</label>
					<label>
						<span className="text-xs text-gray-700">Review content</span>
						<Textarea name="description" value={review.description} required={true} onChange={handleChangeForm} />
					</label>
					<div>
						<span className="text-xs text-gray-700">Rating</span>
						<InputRating name="rating" value={review.rating} onChange={handleChangeForm} />
					</div>
					<label>
						<span className="text-xs text-gray-700">Name</span>
						<Input name="author" value={review.author} required={true} onChange={handleChangeForm} />
					</label>
					<label>
						<span className="text-xs text-gray-700">Email</span>
						<Input name="email" type="email" value={review.email} required={true} onChange={handleChangeForm} />
					</label>
					<button
						type="submit"
						className="mt-6 inline-flex w-full items-center justify-center rounded-md bg-gray-900 px-8 py-2 text-sm font-medium text-gray-50 hover:bg-gray-700 focus:border-blue-300 focus:outline-none focus:ring focus:ring-blue-200 focus:ring-opacity-50"
					>
						Submit review
					</button>
				</form>
			</div>
		</>
	);
};

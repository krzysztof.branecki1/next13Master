import { ProductListItemFragment } from "@/gql/graphql";
import Link from "next/link";

import { ProductItemCoverImage } from "@atoms/ProductItemCoverImage";
import { ProductListItemDescription } from "@atoms/ProductListItemDescription";

export const ProductListItem = ({ product }: { product: ProductListItemFragment }) => {
	// const imageWidth =
	// 	product?.images?.[0] !== null ? product.images[0]?.width : 0;
	// const imageHeight =
	// 	product?.images?.[0] !== null ? product.images[0]?.height : 0;
	return (
		<li key={product.id} className="group relative overflow-hidden">
			<Link href={`/product/${product.slug}`}>
				<article>
					{!!product.images && product.images[0] !== undefined && (
						<ProductItemCoverImage src={product.images[0].url} alt={product.name} width={200} height={200} />
					)}
					<ProductListItemDescription product={product} />
				</article>
			</Link>
		</li>
	);
};

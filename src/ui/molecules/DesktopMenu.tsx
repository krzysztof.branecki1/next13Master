import { MenuItem } from "@layout/Header";

import { NavLink } from "@molecules/NavLink";

export default function DesktopMenu({ menu }: { menu: MenuItem[] }) {
	return (
		<div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
			<div className="flex flex-shrink-0 items-center">
				<img className="h-8 w-auto" src="/next.svg" alt="Your Company" />
			</div>
			<nav role="navigation" className="flex">
				<ul className="hidden sm:ml-6 sm:flex gap-4">
					{menu.map((item) => (
						<li key={item.url.pathname}>
							<NavLink href={item.url}>{item.label}</NavLink>
						</li>
					))}
				</ul>
			</nav>
		</div>
	);
}

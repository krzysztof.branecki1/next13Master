import { deleteUserAction } from "@/actions/deleteUserAction";
import { currentUser } from "@clerk/nextjs";
import { RedirectType, redirect } from "next/navigation";

export const DeleteAccountForm = async () => {
	const user = await currentUser();
	if (!user) {
		throw new Error("User not found");
	}
	const deleteUser = async () => {
		"use server";
		try {
			const response = await deleteUserAction();
			if (response?.deleted) {
				await currentUser();
				redirect("/", RedirectType.replace);
			}
		} catch (error) {
			redirect("/", RedirectType.replace);
		}
	};
	return (
		<form className="flex items-start md:col-span-2" action={deleteUser}>
			<button
				type="submit"
				className="rounded-md bg-red-500 px-3 py-2 text-sm font-semibold  shadow-sm hover:bg-red-400"
			>
				Yes, delete my account
			</button>
		</form>
	);
};

export default function SearchInputFallback() {
	return (
		<form className="flex items-center" action={"search"}>
			<input
				role="searchbox"
				type="text"
				name="query"
				placeholder="Search for products..."
				autoComplete="off"
				className="w-full rounded-lg border bg-white px-4 py-2 text-sm text-black placeholder:text-neutral-500 dark:border-neutral-800"
			/>
			<div className="absolute right-0 top-0 mr-3 flex h-full items-center"></div>
		</form>
	);
}

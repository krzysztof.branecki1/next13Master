import { clerkClient, currentUser } from "@clerk/nextjs";

import { AvatarUploadButton } from "@atoms/AvatarUploadButton";
import { Button } from "@atoms/Button";

export const UpdateProfileForm = async () => {
	const user = await currentUser();

	if (!user) {
		throw new Error("User not found");
	}
	const inputClassName =
		"w-full rounded-lg border bg-white px-4 py-2 text-sm text-black placeholder:text-neutral-500 dark:border-neutral-800";

	const updateUser = async (formData: FormData) => {
		"use server";
		const user = await currentUser();

		if (!user?.id) return;
		const firstName = formData.get("first-name")?.toString() ?? "";
		const lastName = formData.get("last-name")?.toString() ?? "";

		const users = clerkClient.users;
		return await users
			.updateUser(user.id, {
				firstName,
				lastName,
			})
			.then((res) => {
				return res.id;
			});
	};
	const setProfileImage = async (formData: FormData) => {
		"use server";
		console.log(formData.get("avatar"));
		const user = await currentUser();
		if (!user?.id) return;
		const file = formData.get("avatar") as File;
		if (!file) return;
		const users = clerkClient.users;
		users.updateUserProfileImage(user?.id, { file }).catch((err) => {
			console.log(err);
		});
	};
	return (
		<div className="md:col-span-2">
			<div className="grid grid-cols-1 gap-x-6 gap-y-8 sm:max-w-xl sm:grid-cols-6">
				<form className="col-span-full flex items-center gap-x-8" action={setProfileImage}>
					<AvatarUploadButton imageUrl={user.imageUrl} />
				</form>
			</div>
			<form action={updateUser} className="grid grid-cols-1 gap-x-6 gap-y-8 sm:max-w-xl sm:grid-cols-6 mt-12">
				<div className="sm:col-span-3">
					<label htmlFor="first-name" className="block text-sm font-medium leading-6 ">
						First name
					</label>
					<div className="mt-2">
						<input
							type="text"
							name="first-name"
							defaultValue={user.firstName ?? ""}
							id="first-name"
							autoComplete="given-name"
							className={inputClassName}
						/>
					</div>
				</div>

				<div className="sm:col-span-3">
					<label htmlFor="last-name" className="block text-sm font-medium leading-6 ">
						Last name
					</label>
					<div className="mt-2">
						<input
							type="text"
							name="last-name"
							id="last-name"
							defaultValue={user.lastName ?? ""}
							autoComplete="family-name"
							className={inputClassName}
						/>
					</div>
				</div>

				<div className="col-span-full">
					<label htmlFor="email" className="block text-sm font-medium leading-6 ">
						Email address
					</label>
					<div className="mt-2">
						<select className={inputClassName}>
							{user.emailAddresses.map((email) => (
								<option key={email.id} value={email.id}>
									{email.emailAddress}
								</option>
							))}
						</select>
						<button type="button" className="ml-2 rounded-md bg-white/10">
							Add another email
						</button>
					</div>
				</div>

				<div className=" flex">
					<Button type="submit">Save</Button>
				</div>
			</form>
		</div>
	);
};

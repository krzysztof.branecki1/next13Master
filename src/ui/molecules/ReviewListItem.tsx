import { ReviewFragment } from "@/gql/graphql";

import { RatingStars } from "@molecules/RatingStars";

export const ReviewListItem = ({ review }: { review: ReviewFragment }) => {
	return (
		<div className="my-12">
			<div className="flex items-center">
				<div>
					<h4 className="text-sm font-bold text-gray-900">{review.author}</h4>
					<div className="mt-1 flex flex-row items-center gap-2">
						<div aria-hidden="true" className="small-caps text-sm">
							<RatingStars rating={review.rating} />
						</div>
					</div>
				</div>
			</div>
			<div className="">
				<p className="mb-2 mt-4 space-y-6 text-sm font-bold text-gray-600">{review.title}</p>
				<p className="mt-2 text-sm italic text-gray-600">{review.description}</p>
			</div>
		</div>
	);
};

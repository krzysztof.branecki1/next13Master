"use client";

import { deleteOrderItem } from "@/api/deleteOrderItem";
import { useRouter } from "next/navigation";
import { useTransition } from "react";

export function RemoveButton({ orderItemId }: { orderItemId: string }) {
	const router = useRouter();
	const [isPending, startTransition] = useTransition();

	return (
		<button
			disabled={isPending}
			onClick={() =>
				startTransition(async () => {
					await deleteOrderItem(orderItemId);
					router.refresh();
				})
			}
			className="text-sm font-medium text-red-900 hover:text-indigo-500 disabled:cursor-wait disabled:text-slate-400"
		>
			Remove
		</button>
	);
}

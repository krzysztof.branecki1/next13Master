export async function ChangeQuantityFallback({
	quantity,
}: {
	quantity: number;
}) {
	return (
		<form className="flex justify-center">
			<button data-testid="decrement" className="h-6 w-6 border" type="submit">
				-
			</button>
			<span className="w-8 text-center">{quantity}</span>
			<button data-testid="increment" className="h-6 w-6 border" type="submit">
				+
			</button>
		</form>
	);
}

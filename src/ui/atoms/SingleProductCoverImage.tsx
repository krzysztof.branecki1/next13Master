import Image from "next/image";

export const SingleProductCoverImage = ({
	src,
	alt,
	width,
	height,
}: {
	src: string | undefined;
	alt: string | undefined;
	width: number | null | undefined;
	height: number | null | undefined;
}) => {
	if (!src || !width || !height) {
		return null;
	}

	return (
		<Image
			src={src}
			alt={alt ? alt : ""}
			width={100}
			height={100}
			className="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded"
			priority
		/>
	);
	// return (
	// 	<Image
	// 		alt={alt ? alt : ""}
	// 		className="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded"
	// 		src={src}
	// 	/>
	// );
};

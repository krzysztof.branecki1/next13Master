import Image from "next/image";

export const ProductItemCoverImage = ({
	src,
	alt,
	height,
	width,
}: {
	src: string;
	alt: string;
	width: number;
	height: number;
}) => {
	return (
		<div className="aspect-square overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75">
			<Image
				width={width}
				height={height}
				src={src}
				alt={alt}
				className="h-full w-full object-cover object-center  hover:scale-105 transition-all duration-300 ease-in-out"
			/>
		</div>
	);
};

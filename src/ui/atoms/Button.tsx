export const Button = (props: any) => {
	return (
		<button
			className="flex text-white disabled:bg-indigo-300 disabled:cursor-wait bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded"
			{...props}
		>
			{props.children}
		</button>
	);
};

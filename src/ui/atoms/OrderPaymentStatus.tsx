export const OrderPaymentStatus = ({
	paymentStatus,
	_paymentLink,
}: {
	paymentStatus: string;
	_paymentLink?: string;
}) => {
	let status: string;
	let color: string;
	switch (paymentStatus) {
		case "paid":
			status = "Paid";
			color = "bg-green-200";
			break;
		case "refunded":
			status = "Refunded";
			color = "bg-red-200";
			break;
		default:
			status = "Unpaid";
			color = "bg-amber-500";
	}

	return (
		<span className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
			<span aria-hidden className={`absolute inset-0 ${color} opacity-50 rounded-full`}></span>
			<span className="relative">{status}</span>
		</span>
	);
};

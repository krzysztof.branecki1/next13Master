import { ChangeEvent } from "react";

export default function Textarea({
	name,
	placeholder,
	value,
	required,
	onChange,
}: {
	name: string;
	placeholder?: string;
	value: string;
	required?: boolean;
	onChange: (e: ChangeEvent<HTMLTextAreaElement>, name: string) => void;
}) {
	return (
		<textarea
			required={required}
			name={name}
			placeholder={placeholder}
			value={value}
			onChange={(e) => onChange(e, name)}
			className="w-full rounded-lg border bg-white px-4 py-2 text-sm text-black placeholder:text-neutral-500 dark:border-neutral-800"
		/>
	);
}

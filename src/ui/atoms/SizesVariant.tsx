"use client";

// import { ProductListItemFragment } from "@/gql/graphql";
// import { useProductVariablesSearchParams } from "@/hooks/useProductVariablesSearchParams";
// import { ChangeEvent } from "react";
//
// export const SizesVariant = ({ name, variants }: { name: string; variants: ProductListItemFragment["variants"] }) => {
// 	const sizesList = variants.filter((variant) => variant.variantType === "SIZE" && variant.sizeVariant);
// 	const [sizeValue, setSizeValue] = useProductVariablesSearchParams("size");
//
// 	const onChangeSelect = (e: ChangeEvent<HTMLSelectElement>) => {
// 		const value = e.target.value;
// 		setSizeValue(value);
// 	};
//
// 	if (sizesList.length === 0) return;
// 	return (
// 		<div className="flex items-center">
// 			<span className="mr-3">{name}</span>
// 			<div className="relative">
// 				<select
// 					id="productSize"
// 					value={sizeValue ?? ""}
// 					onChange={onChangeSelect}
// 					className="rounded border appearance-none border-gray-300 py-2 focus:outline-none focus:ring-2 focus:ring-indigo-200 focus:border-indigo-500 text-base pl-3 pr-10"
// 				>
// 					{sizesList.map((item) => (
// 						<option key={item.id} value={item.sizeVariant?.size.toLowerCase()}>
// 							{item.sizeVariant?.size}
// 						</option>
// 					))}
// 				</select>
// 				<span className="absolute right-0 top-0 h-full w-10 text-center text-gray-600 pointer-events-none flex items-center justify-center">
// 					<svg
// 						fill="none"
// 						stroke="currentColor"
// 						strokeLinecap="round"
// 						strokeLinejoin="round"
// 						strokeWidth="2"
// 						className="w-4 h-4"
// 						viewBox="0 0 24 24"
// 					>
// 						<path d="M6 9l6 6 6-6"></path>
// 					</svg>
// 				</span>
// 			</div>
// 		</div>
// 	);
// };

"use client";

import { ChangeEvent, useState } from "react";

import { Button } from "@atoms/Button";

export const AvatarUploadButton = async ({ imageUrl }: { imageUrl: string }) => {
	const [userAvatarUrl, setUserAvatarUrl] = useState(imageUrl);
	const handleUpload = async (e: ChangeEvent<HTMLInputElement>) => {
		const file = e.currentTarget.files?.[0];
		if (!file) return;
		const reader = URL.createObjectURL(file);
		setUserAvatarUrl(reader);
	};
	return (
		<>
			<img src={userAvatarUrl} alt="" className="h-24 w-24 flex-none rounded-lg object-cover" />
			<div>
				<div className="mb-3">
					<label htmlFor="formFileMultiple" className="mb-2 inline-block text-neutral-700 dark:text-neutral-200">
						User avatar
					</label>
					<input
						onChange={handleUpload}
						className="relative m-0 block w-full min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-clip-padding px-3 py-[0.32rem] text-base font-normal text-neutral-700 transition duration-300 ease-in-out file:-mx-3 file:-my-[0.32rem] file:overflow-hidden file:rounded-none file:border-0 file:border-solid file:border-inherit file:bg-neutral-100 file:px-3 file:py-[0.32rem] file:text-neutral-700 file:transition file:duration-150 file:ease-in-out file:[border-inline-end-width:1px] file:[margin-inline-end:0.75rem] hover:file:bg-neutral-200 focus:border-primary focus:text-neutral-700 focus:shadow-te-primary focus:outline-none dark:border-neutral-600 dark:text-neutral-200 dark:file:bg-neutral-700 dark:file:text-neutral-100 dark:focus:border-primary"
						type="file"
						name="avatar"
						id="formFileMultiple"
					/>
				</div>
				<p className="mb-2 text-xs leading-5 text-gray-400">JPG, GIF or PNG. 1MB max.</p>
				<Button type="submit">Change avatar</Button>
			</div>
		</>
	);
};

import { ChangeEvent } from "react";

export default function Input({
	name,
	placeholder,
	value,
	required,
	type = "text",
	onChange,
}: {
	name: string;
	placeholder?: string;
	value: string;
	required?: boolean;
	type?: string;
	onChange: (e: ChangeEvent<HTMLInputElement>, name: string) => void;
}) {
	return (
		<input
			type={type}
			required={required}
			name={name}
			placeholder={placeholder}
			value={value}
			onChange={(e) => (onChange ? onChange(e, name) : undefined)}
			className="w-full rounded-lg border bg-white px-4 py-2 text-sm text-black placeholder:text-neutral-500 dark:border-neutral-800"
		/>
	);
}

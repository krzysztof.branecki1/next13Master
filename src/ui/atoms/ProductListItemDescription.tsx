import { ProductListItemFragment } from "@/gql/graphql";

// import { RatingStars } from "@molecules/RatingStars";
import { priceFormat } from "@utils/priceFormat";

type ProductListItemDescriptionProps = {
	product: ProductListItemFragment;
};
export const ProductListItemDescription = ({
	product: { categories, name, price },
}: ProductListItemDescriptionProps) => {
	const formattedPrice = price ? priceFormat(price) : null;
	return (
		<div>
			<div className="mt-2 flex justify-between">
				<h3 className="text-sm font-semibold text-gray-700">{name}</h3>

				{formattedPrice && (
					<p className="text-sm font-medium text-gray-900">
						<span className="sr-only">Price:</span> <span data-testid="product-price">{formattedPrice}</span>
					</p>
				)}
			</div>
			<div className="mt-2 flex justify-between">
				{!!categories &&
					categories.map((category) => {
						return (
							<p key={category.slug} className="text-sm text-gray-500">
								<span className="sr-only">Category:</span> {category.name}
							</p>
						);
					})}
				{/*<RatingStars rating={averageRating} />*/}
			</div>
		</div>
	);
};

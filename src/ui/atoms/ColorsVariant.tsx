"use client";

// import { ProductVariantFragment } from "@/gql/graphql";
// import { useProductVariablesSearchParams } from "@/hooks/useProductVariablesSearchParams";
//
// export const ColorsVariant = ({ name, variants }: { name: string; variants: ProductVariantFragment[] }) => {
// 	const colors = variants.filter((variant) => variant.variantType === "COLOR" && variant.colorVariant);
//
// 	const [colorValue, setColorValue] = useProductVariablesSearchParams("color");
//
// 	if (colors.length === 0) return;
// 	return (
// 		<div className="flex">
// 			<span className="mr-3">{name}</span>
// 			<div className="flex gap-1">
// 				{colors.map((variant, index) => {
// 					const isFirst = index === 0;
// 					const isSetValue = variant.colorVariant?.name?.toLowerCase() === colorValue?.toLowerCase();
// 					const isChecked = isSetValue || (!colorValue && isFirst);
// 					return (
// 						<div className="relative w-6 h-6" key={variant.id}>
// 							<input
// 								className="absolute border-2 border-gray-300 rounded-full w-6 h-6 focus:outline-none"
// 								type="radio"
// 								name={name.toLowerCase()}
// 								id={name.toLowerCase()}
// 								checked={isChecked}
// 								onChange={() => {
// 									if (variant?.colorVariant?.colorCode) setColorValue(variant.colorVariant.name);
// 								}}
// 							/>
// 							<button
// 								style={{
// 									backgroundColor: variant?.colorVariant?.colorCode ? variant.colorVariant.colorCode.css : "",
// 								}}
// 								className={`absolute z-10 ${
// 									isChecked ? "border-gray-500 border-4" : "border-gray-300 border-2"
// 								} rounded-full w-6 h-6 focus:outline-none`}
// 								onClick={() => {
// 									if (variant?.colorVariant?.colorCode) setColorValue(variant.colorVariant.name);
// 								}}
// 							>
// 								<span className="sr-only screen-reader-text">{variant?.colorVariant?.name}</span>
// 							</button>
// 							<span className="sr-only">{variant.name}</span>
// 						</div>
// 					);
// 				})}
// 			</div>
// 		</div>
// 	);
// };

import { getCategories } from "@/api/getCategories";
import { Suspense } from "react";
import { UrlObject } from "url";

import DesktopMenu from "@molecules/DesktopMenu";
import SearchInput from "@molecules/SearchInput";
import SearchInputFallback from "@molecules/SearchInputFallback";
import UserMenu from "@molecules/UserMenu";

import Navigation from "@organisms/Navigation";

export type MenuItem = {
	label: string;
	url: UrlObject;
};
export default async function Header() {
	const categories = await getCategories(3);

	const categoryList =
		categories?.map((category) => ({
			label: category.name,
			url: {
				pathname: `/categories/${category.slug}/1`,
			},
		})) ?? [];

	const menuItems: MenuItem[] = [
		{
			label: "Home",
			url: {
				pathname: "/",
			},
		},
		{
			label: "All",
			url: {
				pathname: "/products/1",
			},
		},
		...categoryList,
	];
	return (
		<Navigation menu={menuItems}>
			<DesktopMenu menu={menuItems} />
			<Suspense fallback={<SearchInputFallback />}>
				<SearchInput />
			</Suspense>
			<UserMenu />
		</Navigation>
	);
}

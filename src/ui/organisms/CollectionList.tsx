import { getCollections } from "@/api/getCollections";

import { CollectionCard } from "@molecules/CollectionCard";

export const CollectionList = async () => {
	const collections = await getCollections(3);
	return (
		<section className="text-gray-600 body-font">
			<div className="container px-5 py-24 mx-auto">
				<div className="flex flex-col text-center w-full mb-20">
					<h2 className="text-xs text-indigo-500 tracking-widest font-medium title-font mb-1">Find your style</h2>
					<h1 className="sm:text-3xl text-2xl font-medium title-font text-gray-900">Our collections</h1>
				</div>
				<div className="flex flex-wrap -m-4">
					{collections.map((collection) => (
						<CollectionCard key={collection.slug} collection={collection} />
					))}
				</div>
			</div>
		</section>
	);
};

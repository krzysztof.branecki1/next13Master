"use client";

import { ReviewFragment } from "@/gql/graphql";
import { useOptimistic } from "react";

import { ReviewForm } from "@molecules/ReviewForm";
import { ReviewListItem } from "@molecules/ReviewListItem";

export const ReviewsList = ({ reviews, productId }: { reviews?: ReviewFragment[]; productId: string }) => {
	if (!reviews) return;
	const [optimisticReviewList, setOptimisticReviewList] = useOptimistic(
		reviews,
		(_state, newQuantity: ReviewFragment[]) => newQuantity,
	);
	const addReviewHandler = (review: Omit<ReviewFragment, "id">) => {
		setOptimisticReviewList([...optimisticReviewList, { ...review, id: Math.random().toString() }]);
	};
	return (
		<div className="mx-auto max-w-2xl lg:grid lg:max-w-7xl lg:grid-cols-12 lg:gap-x-8 lg:py-8">
			<div className="lg:col-span-4 mt-12">
				<ReviewForm addReview={addReviewHandler} productId={productId} />
			</div>
			<div className="mt-16 lg:col-span-7 lg:col-start-6 lg:mt-0">
				<h3 className="sr-only">Recent reviews</h3>
				{reviews.map((review) => (
					<ReviewListItem key={review.id} review={review} />
				))}
			</div>
		</div>
	);
};

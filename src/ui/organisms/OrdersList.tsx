"use client";

import { OrderFragment } from "@/gql/graphql";

import { OrderListItem } from "@molecules/OrderListItem";

export const OrdersList = ({ orders }: { orders: Array<OrderFragment> }) => {
	if (!orders || orders.length < 1) return;
	return (
		<div className="bg-white py-8 rounded-md w-full">
			<div className=" flex items-center justify-between pb-6">
				<div>
					<h2 className="text-gray-600 font-semibold">Products Oder</h2>
					<span className="text-xs">All products item</span>
				</div>
			</div>
			<div>
				<div className="overflow-x-auto">
					<div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
						<table className="min-w-full leading-normal">
							<thead>
								<tr>
									<th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
										Order Number
									</th>
									<th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
										Created at
									</th>
									<th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
										Updated at
									</th>
									<th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
										Cost
									</th>
									<th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
										Items
									</th>
									<th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
										Status
									</th>
								</tr>
							</thead>
							<tbody>
								{orders.map((order: OrderFragment) => {
									return <OrderListItem item={order} key={order.id} />;
								})}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	);
};

import { OrderGetByIdQuery } from "@/gql/graphql";
import Image from "next/image";

import { priceFormat } from "@utils/priceFormat";

export const SidebarCartList = ({ cart }: { cart: OrderGetByIdQuery["order"] }) => {
	const items = cart?.OrderItems;

	if (!items) return null;
	return (
		<ul role="list" className="-my-6 divide-y divide-gray-200">
			{items.map((item, index) => {
				if (!item?.quantity || item?.quantity < 1) return null;
				return (
					<li key={`${item.id}-${index}`} className="flex py-6">
						<div className="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200 flex justify-center items-center">
							{!!item.Product?.images && item.Product?.images[0] !== undefined && (
								<Image src={item.Product?.images[0].url} alt={item.Product?.name} width="64" height="64" />
							)}
						</div>
						<div className="ml-4 flex flex-1 flex-col">
							<div className="flex justify-between text-base font-medium text-slate-900">
								<h3>{item.Product?.name}</h3>
								{item.Product?.price && <p className="small-caps ml-4">{priceFormat(item.Product?.price)}</p>}
							</div>
							{/*{item.Product && <p className="mt-1 text-sm text-slate-500">{item.Product.categories[0]?.name}</p>}*/}
							<div className="flex flex-1 items-end justify-between text-sm font-bold text-slate-500">
								Quantity:{item.quantity}
							</div>
						</div>
					</li>
				);
			})}
		</ul>
	);
};

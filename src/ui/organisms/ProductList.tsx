import { ProductListItemFragment } from "@/gql/graphql";

import { ProductListItem } from "@molecules/ProductListItem";

export const ProductList = ({ products }: { products: ProductListItemFragment[] }) => {
	const productList = products.map((product) => {
		return <ProductListItem key={product.id} product={product} />;
	});
	return (
		<ul data-testid="products-list" className="mt-6 grid grid-cols-1 gap-8 sm:grid-cols-2 lg:grid-cols-4">
			{productList}
		</ul>
	);
};

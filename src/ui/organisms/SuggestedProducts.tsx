import { getProducts } from "@/api/getProducts";
import { searchSuggestedProductsInAgolia } from "@/api/searchSuggestedProductsInAgolia";
import { ProductListItemFragment } from "@/gql/graphql";

import { ProductListItem } from "@molecules/ProductListItem";

export const SuggestedProducts = async ({ product }: { product?: ProductListItemFragment }) => {
	let response;
	if (!product) {
		response = await getProducts(1, 4);
		return (
			<>
				{response && (
					<ul data-testid="related-products" className="mt-6 grid grid-cols-1 gap-8 sm:grid-cols-2 lg:grid-cols-4">
						{response.products.map((product) => (
							<ProductListItem key={product.id} product={product} />
						))}
					</ul>
				)}
			</>
		);
	}
	response = await searchSuggestedProductsInAgolia(product);
	const products = response?.hits;
	return (
		<>
			{response && products && (
				<ul data-testid="related-products" className="mt-6 grid grid-cols-1 gap-8 sm:grid-cols-2 lg:grid-cols-4">
					{products.map((product) => (
						<ProductListItem key={product.id} product={product} />
					))}
				</ul>
			)}
		</>
	);
};

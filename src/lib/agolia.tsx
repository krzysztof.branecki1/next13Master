import { getProductById } from "@/api/getProductById";
import { getProducts } from "@/api/getProducts";
import algoliasearch from "algoliasearch";

export const getAgoliaIndex = (index: string) => {
	if (process.env.NEXT_PUBLIC_ALGOLIA_APP_ID === undefined) {
		throw new Error("ALGOLIA_APP_ID is not defined");
	}
	if (process.env.ALGOLIA_ADMIN_API_KEY === undefined) {
		throw new Error("ALGOLIA_ADMIN_API_KEY is not defined");
	}

	const algolia = algoliasearch(process.env.NEXT_PUBLIC_ALGOLIA_APP_ID, process.env.ALGOLIA_ADMIN_API_KEY);
	return algolia.initIndex(index);
};

const index = getAgoliaIndex("products");
export const indexAllProducts = async () => {
	const response = await getProducts(1, 99999);
	const records = response.products.map((product) => {
		return { objectID: product.id, ...product };
	});
	return index.saveObjects(records).wait();
};

export const indexProduct = async (productId: string) => {
	const product = await getProductById(productId);
	if (product) {
		const record = { objectID: product.id, ...product };
		await index.saveObject(record).wait();
	}
	return product;
};

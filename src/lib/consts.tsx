import { ProductOrderByInput } from "@/gql/graphql";

export const defaultSortOrder = ProductOrderByInput.NameAsc;
export const PER_PAGE = 4;

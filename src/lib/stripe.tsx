import Stripe from "stripe";

export const getStripeCheckoutSession = async (sessionId: string) => {
	if (!process.env.STRIPE_SECRET_KEY) {
		throw new Error("STRIPE_SECRET_KEY not set");
	}
	const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {
		apiVersion: "2023-10-16",
		typescript: true,
	});

	return await stripe.checkout.sessions.retrieve(sessionId);
};

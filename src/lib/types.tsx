import { ProductListItemFragment } from "@/gql/graphql";

export type HygraphHookRequest = {
	operation: string;
	data?: {
		id: string;
		slug: string;
		product?: ProductListItemFragment;
	};
};

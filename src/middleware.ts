import { authMiddleware } from "@clerk/nextjs";

// This example protects all routes including api/trpc routes
// Please edit this to allow other routes to be public as needed.
// See https://clerk.com/docs/references/nextjs/auth-middleware for more information about configuring your middleware

export const config = {
	matcher: ["/((?!.+\\.[\\w]+$|_next).*)", "/", "/(api|trpc)(.*)"],
};

export default authMiddleware({
	// @ts-ignore
	publicRoutes: [
		"/",
		"/:filename",
		"/search",
		"/cart",
		"/cart/sidebar",
		"/categories/:categorySlug/:pageNumber",
		"/collections/:collectionSlug",
		"/collections/:collectionSlug/:pageNumber",
		"/product/:productSlug",
		"/products",
		"/products/:pageNumber",
	],
	ignoredRoutes: ["/api/(.*)"],
});

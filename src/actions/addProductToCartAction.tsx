import { addProductToCart } from "@/api/addProductToCart";

import { getOrCreateCart } from "@utils/getOrCreateCart";

export async function addProductToCartAction(formData: FormData) {
	"use server";

	const productId = formData.get("productId");
	if (productId) {
		const cart = await getOrCreateCart();
		if (cart) {
			await addProductToCart(cart, productId.toString());
		}
	}
}

"use server";

import { DeletedObject } from "@clerk/backend";
import { clerkClient, currentUser } from "@clerk/nextjs";

export const deleteUserAction = async () => {
	const user = await currentUser();

	if (!user?.id) return;

	const users = clerkClient.users;
	return (await users.deleteUser(user.id)) as unknown as DeletedObject;
};

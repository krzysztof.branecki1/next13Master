import { getCart } from "@/api/getCart";
import { updateOrder } from "@/api/order/updateOrder";
import { PaymentStatus } from "@/gql/graphql";
import { auth, clerkClient } from "@clerk/nextjs";
import { cookies, headers } from "next/headers";
import { redirect } from "next/navigation";
import { Stripe } from "stripe";

export async function handleStripePaymentAction() {
	"use server";
	if (!process.env.STRIPE_SECRET_KEY) {
		throw new Error("Missing STRIPE_SECRET_KEY env variable");
	}

	const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {
		apiVersion: "2023-10-16",
		typescript: true,
	});

	const cart = await getCart();
	if (!cart) {
		return;
	}

	if (!cart?.OrderItems) {
		return;
	}
	const clerkSession = auth();

	const user = clerkSession.userId ? await clerkClient.users.getUser(clerkSession.userId) : undefined;

	const headerList = headers();
	const host = headerList.get("host");

	const orderItems = cart.OrderItems.filter((orderItem) => {
		return orderItem !== null;
	});
	const userEmail = user?.emailAddresses[0]
		? user?.emailAddresses.find((email) => {
				return email.id === user?.primaryEmailAddressId;
		  })
		: undefined;
	const session = await stripe.checkout.sessions
		.create({
			metadata: {
				customer_id: cart.Customer?.id ?? null,
				order_id: cart.id,
			},
			customer_email: userEmail ? userEmail.emailAddress : undefined,
			payment_intent_data: {
				metadata: {
					order_id: cart.id,
				},
			},
			payment_method_types: ["card", "p24", "blik"],
			currency: "pln",
			line_items: orderItems.map((orderItem) => {
				return {
					price_data: {
						currency: "pln",
						unit_amount: orderItem?.Product?.price ? orderItem?.Product?.price * 100 : undefined,
						product_data: {
							name: "orderItem.Product",
							description: orderItem?.Product?.description,
							images: orderItem?.Product?.images ? orderItem.Product.images.map((i) => i.url) : undefined,
						},
					},
					quantity: orderItem?.quantity,
				};
			}),
			mode: "payment",
			success_url: `http://${host}/cart/success?session_id={CHECKOUT_SESSION_ID}`,
			cancel_url: `http://${host}/cart/canceled?session_id={CHECKOUT_SESSION_ID}`,
		})
		.then(async (session) => {
			await updateOrder(cart.id, {
				stripeCheckoutId: session.id,
				total: session.amount_subtotal,
				status: session.payment_status as PaymentStatus,
			});
			cookies().delete("cartId");
			return session;
		});
	if (session.url) {
		redirect(session.url);
	}
}

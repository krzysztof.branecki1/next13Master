"use server";

import { ReviewAddToProductDocument } from "@/gql/graphql";
import { executeGraphql } from "@/lib/executeGraphql";
import { revalidatePath } from "next/cache";

export default async function addReviewAction(formData: FormData) {
	const author = formData.get("author")?.toString() ?? "";
	const email = formData.get("email")?.toString() ?? "";
	const ratingFromFormData = formData.get("rating")?.toString() ?? null;
	const rating = ratingFromFormData ? parseInt(ratingFromFormData) : 1;
	const description = formData.get("description")?.toString() ?? "";
	const title = formData.get("title")?.toString() ?? "";
	const productId = formData.get("productId")?.toString() ?? "";

	const response = await executeGraphql({
		query: ReviewAddToProductDocument,
		variables: {
			review: {
				author,
				email,
				rating,
				description,
				title,
				Product: {
					connect: { id: productId },
				},
			},
		},
		headers: { authorization: `Bearer ${process.env.HYGRAPH_MUTATION_TOKEN}` },
	});
	if (response.createReview?.Product?.slug) {
		revalidatePath(`/product/${response.createReview?.Product?.slug}`);
	}
}

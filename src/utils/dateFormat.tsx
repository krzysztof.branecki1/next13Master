export const dateFormat = (date: string | unknown) => {
	if (!(typeof date === "string")) return;
	if (!date || isNaN(Date.parse(date))) return;
	const dateObject = new Date(date);
	return dateObject.toLocaleDateString("pl-PL");
};

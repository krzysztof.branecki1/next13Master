import { OrderItemFragment } from "@/gql/graphql";

import { priceFormat } from "@utils/priceFormat";

export const totalOrderCost = (orderItems: OrderItemFragment[]) => {
	if (!orderItems) return;
	// const total = orderItems.reduce((acc, item) => acc + item.quantity * item.total, 0);
	const total = orderItems.reduce((acc, item) => acc + item.quantity * 1, 0);
	return priceFormat(total);
};

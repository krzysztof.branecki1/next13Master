import { createCart } from "@/api/createCart";
import { getCart } from "@/api/getCart";
import { OrderGetByIdQuery } from "@/gql/graphql";
import { cookies } from "next/headers";

export async function getOrCreateCart(): Promise<OrderGetByIdQuery["order"]> {
	const cart = await getCart();
	if (cart?.id) {
		return cart;
	} else {
		cookies().delete("cartId");
		const cart = await createCart();
		if (cart?.id) {
			cookies().set("cartId", cart.id, {
				sameSite: "lax",
				httpOnly: true,
			});
			return cart;
		}
	}

	throw new Error("Failed to create cart");
}
